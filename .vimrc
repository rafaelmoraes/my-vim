""""""""""""""""""""""""""""""""" LOAD PLUGINS """""""""""""""""""""""""""""""""
"PATHOGEN
execute pathogen#infect()
filetype plugin indent on



"""""""""""""""""""""""""""""""""""" STYLE """""""""""""""""""""""""""""""""""""
syntax enable
colorscheme solarized

set tabstop=2
set shiftwidth=2
set expandtab
set number
set hlsearch "highlight match pattern founds
set cursorline
set background=light
set guifont=Roboto\ Mono\ for\ Powerline\ 10
set antialias
set colorcolumn=80
set guioptions -=T
"set cursorcolumn



"""""""""""""""""""""""""""""""""" PLUGINS """""""""""""""""""""""""""""""""""""
"CTRLP

"SYNTASTIC
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"VIM-AIRLINE
let &t_Co=256
set laststatus=2
let g:airline_theme='solarized'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

"VIM-AIRLINE-THEMES

"NERDTREE
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
map <C-n> :NERDTreeToggle<CR>

"NERDTREE-GIT-PLUGIN

"VIM-FUGITIVE

"VIM-GITGUTTER
set updatetime=250

"NEOCOMPLETE
let g:neocomplete#enable_at_startup = 1

"NEOSNIPPET-VIM
let g:neosnippet#snippets_directory='~/.vim/bundle/neosnippet-snippets/neosnippets'

"NEOSNIPPET-SNIPPETS

"VIM-JAVASCRIPT
let g:javascript_plugin_flow = 1

"VIM-FLOW INCOMPLETO

"EMMET
"let g:user_emmet_mode='a'    "enable all function in all mode.
"let g:user_emmet_install_global = 1
"let g:user_emmet_leader_key='<C-Y>'

"VIM-RUBY

"VIM-ENDWISE

"RAILS

"BUNDLER

"DELIMITMATE

"INDENTLINE
let g:indentLine_enabled = 0

"VIM-MULTIPLE-CURSORS

"VIM-MARKDOWN

"VIM-ABOLISH



"""""""""""""""""""""""""""""""""" SHOTCUTS """"""""""""""""""""""""""""""""""""
noremap  <C-S> :update<CR>
vnoremap <C-S> <C-C>:update<CR>
inoremap <C-S> <Esc>:update<CR>

noremap  <C-x> dd<CR>
vnoremap <C-x> d<CR>
inoremap <C-x> <Esc> dd<CR>

noremap  <C-c> y<CR>
vnoremap <C-c> y<CR>
inoremap <C-c> <Esc> y<CR>

noremap  <C-v> p<CR>
vnoremap <C-v> p<CR>
inoremap <C-v> <Esc> p<CR>

noremap  <C-a> ggVG<CR>
vnoremap <C-a> ggVG<CR>
inoremap <C-a> <Esc> ggVG<CR>

nnoremap <C-z>  :undo<CR>
inoremap <C-z>  <Esc>:undo<CR>
nnoremap <C-y>  :redo<CR>
inoremap <C-y> <Esc>:redo<CR>
